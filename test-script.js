import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
  duration: '5m',
  vus: 100,
};

export default function () {
  http.get('https://dental-clinic.got.co.th/');
  sleep(3);
}
