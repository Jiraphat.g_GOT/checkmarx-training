FROM php:7.0-apache
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN apt update && apt install -y vim
ADD app/admin /var/www/html/admin
ADD app/css /var/www/html/css
ADD app/font /var/www/html/font
ADD app/images /var/www/html/images
ADD app/img /var/www/html/img
ADD app/js /var/www/html/js
ADD app/media /var/www/html/media
ADD app/scripts /var/www/html/scripts
COPY app/*.php /var/www/html/
COPY app/*.ttf /var/www/html/
RUN php datadog-setup.php --php-bin=all --enable-appsec